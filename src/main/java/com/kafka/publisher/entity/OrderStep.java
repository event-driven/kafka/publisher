package com.kafka.publisher.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kafka.publisher.enums.Process;
import com.kafka.publisher.enums.StepStatus;

import javax.persistence.*;

@Entity
public class OrderStep {
    @Id
    @GeneratedValue(generator = "seq_orders_step")
    private Long id;
    private Process process;
    @Enumerated(EnumType.ORDINAL)
    private StepStatus status;
    @ManyToOne
    @JoinColumn(name = "order_id")
    @JsonIgnore
    private Order order;

    public OrderStep(Process process){
        this.process = process;
    }

    public OrderStep(){}

    @PrePersist
    public void init(){
        this.status = StepStatus.PENDING;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public StepStatus getStatus() {
        return status;
    }

    public void setStatus(StepStatus status) {
        this.status = status;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
