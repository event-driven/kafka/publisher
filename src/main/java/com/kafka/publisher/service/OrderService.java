package com.kafka.publisher.service;

import com.kafka.publisher.entity.Order;
import com.kafka.publisher.request.OrderRequest;

public interface OrderService {
    Order createOrder(OrderRequest orderRequest);
}
