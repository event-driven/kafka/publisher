package com.kafka.publisher.service.impl;

import com.kafka.publisher.entity.Order;
import com.kafka.publisher.enums.OrderState;
import com.kafka.publisher.mapper.OrderMapper;
import com.kafka.publisher.repository.OrderRepository;
import com.kafka.publisher.request.OrderRequest;
import com.kafka.publisher.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private KafkaTemplate<String, String> template;

    @Value(value = "${spring.kafka.topic.name}")
    private String topic;

    @Override
    public Order createOrder(OrderRequest orderRequest) {
        Order order = orderRepository.save(OrderMapper.requestToEnity(orderRequest));
        order.setState(OrderState.PROCESSING);
        orderRepository.save(order);
        template.send(topic, String.valueOf(order.getId()), String.valueOf(order.getId()));
        return order;
    }
}