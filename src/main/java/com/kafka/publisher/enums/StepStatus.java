package com.kafka.publisher.enums;

public enum StepStatus {
    PENDING,
    PROCESSING,
    FAIL,
    SUCCESS;
}
