package com.kafka.publisher.enums;

public enum OrderState {
    PENDING,
    PROCESSING,
    FAILED,
    SUCCESS;
}
