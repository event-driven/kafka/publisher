package com.kafka.publisher.mapper;

import com.kafka.publisher.request.OrderRequest;
import com.kafka.publisher.entity.Order;

public class OrderMapper {

    public static Order requestToEnity(OrderRequest request){
        Order order = new Order();
        order.setProductName(request.getProductName());
        order.setAmount(request.getAmount());
        return order;
    }

}
