package com.kafka.publisher.api;

import com.kafka.publisher.repository.OrderRepository;
import com.kafka.publisher.request.OrderRequest;
import com.kafka.publisher.entity.Order;
import com.kafka.publisher.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;

    @PostMapping
    public ResponseEntity<?> createOrder(@RequestBody OrderRequest request){
        Order order = orderService.createOrder(request);
        System.out.println(String.format("-Created %s\n-And send to kafka topics for next business process", order.toString()));
        return ResponseEntity.ok(order);
    }

    @GetMapping
    public Order get(@RequestParam("id") Long id){
        return orderRepository.getById(id);
    }

}
